﻿
namespace Sea_battle
{
    public class Player
    {
        public int Score { get; set; }

        public Map mainMap = new Map();

        public Map fireMap = new Map();

        public Player()
        {
            mainMap.SetMap();

            fireMap.SetMap();
        }

        public void PrintMaps()
        {
            fireMap.PrintMap();

            mainMap.PrintMap();
        }
    }

}
