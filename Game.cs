﻿using System;

namespace Sea_battle
{
    public class Game
    {
        public Player player1 = new Player();

        public Player player2 = new Player();

        public void Fire(Player playerFire, Player playerDamage, int x, int y)
        {
            if (playerDamage.mainMap.array[y, x] == 'X')
            {
                playerFire.Score += 1;

                Console.WriteLine("Попал");

                playerFire.fireMap.array[y, x] = 'K';

                playerDamage.mainMap.array[y, x] = ' ';
            }
            else
            {
                playerFire.fireMap.array[y, x] = 'M';
            }
        }
    }
}
