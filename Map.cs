﻿using System;

namespace Sea_battle
{
    public class Map
    {
        public char[,] array = new char[11, 12];

        public void SetMap()
        {
            array[1, 0] = '1';
            array[2, 0] = '2';
            array[3, 0] = '3';
            array[4, 0] = '4';
            array[5, 0] = '5';
            array[6, 0] = '6';
            array[7, 0] = '7';
            array[8, 0] = '8';
            array[9, 0] = '9';
            array[10, 0] = '1';
            array[10, 1] = '0';

            array[0, 2] = 'A';
            array[0, 3] = 'B';
            array[0, 4] = 'C';
            array[0, 5] = 'D';
            array[0, 6] = 'E';
            array[0, 7] = 'F';
            array[0, 8] = 'G';
            array[0, 9] = 'H';
            array[0, 10] = 'I';
            array[0, 11] = 'J';
        }

        public void PrintMap()
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    Console.Write(array[i, j]);
                }

                Console.WriteLine();
            }
        }

        public bool TrySetShip((int, char) point1, (int, char) point2)
        {
            bool result = true;

            int x1 = 0, x2 = 0;

            switch (point1.Item2
                .ToString()
                .ToUpper()
                .ToCharArray()[0])
            {
                case 'A':
                    x1 = 2;
                    break;

                case 'B':
                    x1 = 3;
                    break;

                case 'C':
                    x1 = 4;
                    break;

                case 'D':
                    x1 = 5;
                    break;

                case 'E':
                    x1 = 6;
                    break;

                case 'F':
                    x1 = 7;
                    break;

                case 'G':
                    x1 = 8;
                    break;

                case 'H':
                    x1 = 9;
                    break;

                case 'I':
                    x1 = 10;
                    break;

                case 'J':
                    x1 = 11;
                    break;
            }

            switch (point2.Item2.
                ToString()
                .ToUpper()
                .ToCharArray()[0])
            {
                case 'A':
                    x2 = 2;
                    break;

                case 'B':
                    x2 = 3;
                    break;

                case 'C':
                    x2 = 4;
                    break;

                case 'D':
                    x2 = 5;
                    break;

                case 'E':
                    x2 = 6;
                    break;

                case 'F':
                    x2 = 7;
                    break;

                case 'G':
                    x2 = 8;
                    break;

                case 'H':
                    x2 = 9;
                    break;

                case 'I':
                    x2 = 10;
                    break;

                case 'J':
                    x2 = 11;
                    break;

            }

            if (point1.Item1 == point2.Item1)
            {
                for (int i = x1; i <= x2; i++)
                {
                    if (!IsCellFree(point1.Item1, i))
                    {
                        result = false;
                    }
                }
            }
            else if (x1 == x2)
            {
                for (int i = point1.Item1; i <= point2.Item1; i++)
                {
                    if (!IsCellFree(i, x1))
                    {
                        result = false;
                    }
                }
            }

            if (result)
            {
                if (point1.Item1 == point2.Item1)
                {
                    for (int i = x1; i <= x2; i++)
                    {
                        SetShip(i, point1.Item1);
                    }
                }
                else if (x1 == x2)
                {
                    for (int i = point1.Item1; i <= point2.Item1; i++)
                    {
                        SetShip(x1, i);
                    }
                }

            }

            return result;
        }

        public bool TrySetShip((int, char) point1)
        {
            int x = 0;

            switch (point1.Item2.ToString().ToUpper().ToCharArray()[0])
            {
                case 'A':
                    x = 2;
                    break;

                case 'B':
                    x = 3;
                    break;

                case 'C':
                    x = 4;
                    break;

                case 'D':
                    x = 5;
                    break;

                case 'E':
                    x = 6;
                    break;

                case 'F':
                    x = 7;
                    break;

                case 'G':
                    x = 8;
                    break;

                case 'H':
                    x = 9;
                    break;

                case 'I':
                    x = 10;
                    break;

                case 'J':
                    x = 11;
                    break;

            }

            if (IsCellFree(point1.Item1, x))
            {
                SetShip(x, point1.Item1);
            }

            return IsCellFree(point1.Item1, x);
        }

        public void SetShip(int x, int y)
        {
            array[y, x] = 'X';
        }

        public bool IsCellFree(int x, int y)
        {
            if ((x + 2) == array.GetLength(1)
                && y == array.GetLength(0))
            {
                if (array[x - 1, y] == 'X'
                    || array[x, y - 1] == 'X')
                {
                    return false;
                }
            }
            else if ((x + 2) == array.GetLength(1))
            {
                if (array[x - 1, y] == 'X'
                    || array[x, y + 1] == 'X'
                    || array[x, y - 1] == 'X')
                {
                    return false;
                }
            }
            else if (y == array.GetLength(0))
            {
                if (array[x + 1, y] == 'X'
                    || array[x - 1, y] == 'X'
                    || array[x, y - 1] == 'X')
                {
                    return false;
                }
            }
            else
            {
                if (array[x + 1, y] == 'X'
                    || array[x - 1, y] == 'X'
                    || array[x, y + 1] == 'X'
                    || array[x, y - 1] == 'X')
                {
                    return false;
                }
            }

            return true;
        }
    }
}
