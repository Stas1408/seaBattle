﻿using System;

namespace Sea_battle
{
    class UI
    {
        private Game _game;

        private static char[] _letters;

        public UI(Game game)
        {
            _game = game;

            _letters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };
        }

        public void Start()
        {
            SayHello();

            SetShips();

            //RandomSetShips(game.player1.mainMap);
            RandomSetShips(_game.player2.mainMap);

            StartGame();
        }

        public static void SayHello()
        {
            Console.WriteLine("Добро пожаловать");
            Console.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }

        public void StartGame()
        {
            while (_game.player1.Score != 20 || _game.player2.Score != 20)
            {
                UpdateMap();

                Console.WriteLine("Ваш счет: " + _game.player1.Score);
                Console.WriteLine("Нажмите любую клавишу...");

                Console.ReadKey();

                ReadFire();

                //RandomFire(game.player1, game.player2);   
                RandomFire(_game.player2, _game.player1);
            }
        }

        public void SetShips()
        {
            while (true)
            {
                Console.WriteLine("Введите коородинаты корабля");
                Console.WriteLine("Y1 корабля(1..10)");

                int y1 = CheckY();

                Console.WriteLine("X1 корабля(A..J)");

                char x1 = CheckX();

                Console.WriteLine("Y2 корабля(1..10)");

                int y2 = CheckY();

                Console.WriteLine("X2 корабля(A..J)");

                char x2 = CheckX();

                _game.player1.mainMap.TrySetShip((y1, x1), (y2, x2));

                Console.WriteLine("Желаете добавит еще?(Пустая строка - да)");

                {
                    if (Console.ReadLine() != string.Empty)
                    {
                        break;
                    }
                }
            }
        }

        public static int CheckY()
        {
            int result;

            while (!int.TryParse(Console.ReadLine(), out result)
                || result <= 0 || result > 10)
            {
                Console.WriteLine("Некорректный ввод");
            }

            return result;
        }

        public static char CheckX()
        {
            string str = Console.ReadLine();

            str = str.ToUpper();

            bool letterTrue = false;

            for (int i = 0; i < _letters.Length; i++)
            {
                if (_letters[i] == str[0])
                {
                    letterTrue = true;
                }
            }

            Console.WriteLine(str + "" + str.Length);

            while (str.Length != 1 || !letterTrue)
            {
                Console.WriteLine("Некорректный ввод");

                str = Console.ReadLine();

                str = str.ToUpper();

                for (int i = 0; i < _letters.Length; i++)
                {
                    if (_letters[i] == str[0])
                    {
                        letterTrue = true;
                    }
                }
            }

            return str.ToCharArray()[0];
        }

        public void ReadFire()
        {
            Console.WriteLine("Y атаки(1..10)");

            int y = CheckY();

            Console.WriteLine("X атаки(A..J)");

            char x = CheckX();

            int x1 = -1;

            switch (x)
            {
                case 'A':
                    x1 = 2;
                    break;

                case 'B':
                    x1 = 3;
                    break;

                case 'C':
                    x1 = 4;
                    break;

                case 'D':
                    x1 = 5;
                    break;

                case 'E':
                    x1 = 6;
                    break;

                case 'F':
                    x1 = 7;
                    break;

                case 'G':
                    x1 = 8;
                    break;

                case 'H':
                    x1 = 9;
                    break;

                case 'I':
                    x1 = 10;
                    break;

                case 'J':
                    x1 = 11;
                    break;
            }

            _game.Fire(_game.player1, _game.player2, x1, y);
        }

        public void RandomFire(Player player1, Player player2)
        {
            Random rnd = new Random();

            _game.Fire(player1, player2, rnd.Next(2, 12), rnd.Next(1, 10));
        }

        public void UpdateMap()
        {
            Console.Clear();

            _game.player1.PrintMaps();
        }

        public void RandomSetShips(Map map)
        {
            CreatRandomShip(1, 4, map);

            CreatRandomShip(2, 3, map);

            CreatRandomShip(3, 2, map);

            CreatRandomShip(4, 1, map);
        }

        public void CreatRandomShip(int lenght, int quantity, Map map)
        {
            Random rnd = new Random();

            bool findDirection = false;

            for (int i = 0; i < quantity; i++)
            {
                while (!findDirection)
                {
                    int y = rnd.Next(1, 10), x1 = -1;

                    int x = rnd.Next(0, 9), y1 = -1;

                    switch (rnd.Next(1, 4))
                    {
                        case 1:
                            if (x <= 10 - lenght)
                            {
                                x1 = x + lenght - 1;
                                y1 = y;
                            }
                            break;

                        case 2:
                            if (x >= lenght - 1)
                            {
                                x1 = x;
                                x = x1 - lenght + 1;
                                y1 = y;
                            }
                            break;

                        case 3:
                            if (y <= 11 - lenght)
                            {
                                y1 = y + lenght - 1;
                                x1 = x;
                            }
                            break;

                        case 4:
                            if (y >= 0 + lenght)
                            {
                                y1 = y;
                                y = y1 - lenght;
                                x1 = x;
                            }
                            break;
                    }

                    if (x1 != -1 
                        && y1 != -1)
                    {
                        findDirection = map.TrySetShip((y, _letters[x]), (y1, _letters[x1]));
                    }
                }

                findDirection = false;
            }
        }
    }
}
